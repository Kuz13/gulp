let gulp = require('gulp');
let browserSync = require('browser-sync').create();
let sass = require('gulp-sass');
gulp.task('test1', function(){
    console.log('test1');
})
gulp.task('test2', function(){
    console.log('test2');
})
gulp.task('test-all', ['test1', 'test2'], function(){
    console.log('all tests passed!');
})
gulp.task('copy-html', function(){
    gulp.src('./src/**/*.html').pipe(gulp.dest('./dist/'));
})
gulp.task('sass', function(){
    gulp.src('./src/scss/**/*.scss').pipe(sass()).pipe(gulp.dest('./dist/css/'));
});
gulp.task('browser-sync', ['copy-html'], function() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    gulp.watch("./src/**/*.html", ['copy-html']).on('change', browserSync.reload);
});